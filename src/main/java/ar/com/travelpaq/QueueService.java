package ar.com.travelpaq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
// Java Core and Utils import
import java.util.List;

// AWS SDK
import com.amazon.sqs.javamessaging.ExtendedClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazon.sqs.javamessaging.AmazonSQSExtendedClient;

// AWS Models
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.ListQueuesRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

public class QueueService 
{
    private Regions region;
    private AmazonS3 s3client;
    private AmazonSQS sqsExtendedClient;

	/**
	 *  Constructor del servicio de colas
     *  @param regionName nombre de la region de AWS
	 */
	public QueueService(String regionName) {
	    region = Regions.fromName(regionName);

        AWSCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();

	    // Creamos el cliente comun para SQS
        AmazonSQS sqsClient = AmazonSQSClient
                .builder()
                .withCredentials(credentialsProvider)
                .withRegion(region)
                .build();

		// Crear el cliente para S3
        s3client = AmazonS3Client
                .builder()
                .withCredentials(credentialsProvider)
                .withRegion(region)
                .build();

        // Configuramos el cliente extendido con el soporte para mensajes > 256KB habilitado
        ExtendedClientConfiguration extendedClientConfig = new ExtendedClientConfiguration()
                .withLargePayloadSupportEnabled(s3client, "travelpaq-sqs-test-bucket")
                .withAlwaysThroughS3(false);
        sqsExtendedClient = new AmazonSQSExtendedClient(sqsClient, extendedClientConfig);
	}
	
	/**
	 * Devuelve una lista de las URLs de las colas existenes
	 * @return  lista con las URLs.
	 */
	public List<String> getQueuesURLs() {
	    return sqsExtendedClient.listQueues().getQueueUrls();
	}
	
	/**
	 * Devuelve una lista de las URLs de las colas cuyos nombres comienzan con un prefijo especificado
	 * @param  	prefix  el prefijo para realizar la búsqueda
	 * @return 	        lista con las URL que coinciden con el prefijo
	 */
	public List<String> getQueuesURLs(String prefix) {
		return sqsExtendedClient.listQueues(new ListQueuesRequest(prefix)).getQueueUrls();
	}
	
	/**
	 * Devuelve la URL de una cola
	 * @param queueName el nombre de la cola para la cual retornar la URL
     * @return          la URL de la cola especificada
	 */
	public String getQueueURL(String queueName) {
	    return sqsExtendedClient.getQueueUrl(queueName).getQueueUrl();
	}

	/**
	 *  Elimna una cola
     *  @param queueUrl la URL de la cola a eliminar
	 */
	public void deleteQueueByURL(String queueUrl) {
		DeleteQueueRequest delete_request = new DeleteQueueRequest(queueUrl);
        sqsExtendedClient.deleteQueue(delete_request);
	}

	/**
	 *  Crea una nueva cola
     *  @param queueName nombre de la cola a crear
     *  @return resultado de creacion de la cola
	 */
	public String createQueue(String queueName) {
		CreateQueueRequest create_request = new CreateQueueRequest(queueName);
		CreateQueueResult result = sqsExtendedClient.createQueue(create_request);
		return result.getQueueUrl();
	}
	
	/**
	 * Enviar un mensaje a una cola
     * @param queueURL la URL de la cola a la cual se envia el mensaje
     * @param message el mensaje a enviar
     * @param delaySeconds tiempo de espera antes de enviar el mensaje
	 */
	public HashMap<String, String> sendMessage(String queueURL, String message, int delaySeconds) {
		SendMessageRequest send_msg_request = new SendMessageRequest()
		        .withQueueUrl(queueURL)
		        .withMessageBody(message)
		        .withDelaySeconds(delaySeconds);
		SendMessageResult result = sqsExtendedClient.sendMessage(send_msg_request);
		
		// Convertimos el resultado a un hashmap
        HashMap<String, String> resultHash = new HashMap<String, String>();
		resultHash.put("MD5OfMessageBody", result.getMD5OfMessageBody());
		resultHash.put("MessageId", result.getMessageId());
		return resultHash;
	}
	
	/**
	 *  Recibir mensajes de una cola
     *  @param queueURL la URL de la cola sobre la cual se desean recibir mensajes
     *  @return lista con los mensajes recibidos
	 */
	public ArrayList<HashMap<String, String>> receiveMessages(String queueURL) {
		List<Message> messages = sqsExtendedClient.receiveMessage(queueURL).getMessages();
		
		// Convertimos el resultado a un hashmap
		ArrayList<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		for(Message message : messages) {
            HashMap<String, String> resultItem = new HashMap<String, String>();
			resultItem.put("id", message.getMessageId());
			resultItem.put("receiptHandle", message.getReceiptHandle());
			resultItem.put("body", message.getBody());
			resultList.add(resultItem);
		}
		return resultList;
	}
	
	/**
	 *  Eliminar un mensaje de una cola
     *  @param queueURL la URL de la cola sobre la cual se desean eliminar un mensaje
     *  @param receiptHandle el handle del mensaje a ser eliminado
	 */
	public void deleteMessage(String queueURL, String receiptHandle) {
        sqsExtendedClient.deleteMessage(queueURL, receiptHandle);
	}
}